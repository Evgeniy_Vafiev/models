<?php

namespace Entities;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\Bestseller
 *
 * @property int $id
 * @property int $sku_id
 * @property int $position
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property-read ItemSku $itemSku
 * @method static Builder|Bestseller newModelQuery()
 * @method static Builder|Bestseller newQuery()
 * @method static Builder|Bestseller query()
 * @method static Builder|Bestseller whereCreatedAt($value)
 * @method static Builder|Bestseller whereId($value)
 * @method static Builder|Bestseller wherePosition($value)
 * @method static Builder|Bestseller whereSkuId($value)
 * @method static Builder|Bestseller whereUpdatedAt($value)
 * @mixin Eloquent
 */
class Bestseller extends Model
{
    protected $table = 'bestsellers';
    protected $primaryKey = 'id';

    public const ID_COLUMN = 'id';
    public const SKU_ID_COLUMN = 'sku_id';
    public const POSITION_COLUMN = 'position';

    public $timestamps = false;

    protected $fillable = [
        self::SKU_ID_COLUMN,
        self::SKU_ID_COLUMN,
        self::POSITION_COLUMN
    ];

    public function itemSku()
    {
        return $this->belongsTo(ItemSku::class, ItemSku::ID_COLUMN, self::SKU_ID_COLUMN);
    }
}
