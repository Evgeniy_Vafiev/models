<?php

namespace Entities;

use Eloquent;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Entities\MainPageBlock
 *
 * @property int $id
 * @property string|null $name
 * @property string $picture
 * @property string|null $description
 * @property string|null $description_position
 * @property string|null $description_color
 * @property string $url
 * @property string|null $anchor_link
 * @property int $position
 * @property bool|null $show
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|MainPageBlock newModelQuery()
 * @method static Builder|MainPageBlock newQuery()
 * @method static Builder|MainPageBlock query()
 * @method static Builder|MainPageBlock whereAnchorLink($value)
 * @method static Builder|MainPageBlock whereCreatedAt($value)
 * @method static Builder|MainPageBlock whereDescription($value)
 * @method static Builder|MainPageBlock whereDescriptionColor($value)
 * @method static Builder|MainPageBlock whereDescriptionPosition($value)
 * @method static Builder|MainPageBlock whereId($value)
 * @method static Builder|MainPageBlock whereName($value)
 * @method static Builder|MainPageBlock wherePicture($value)
 * @method static Builder|MainPageBlock wherePosition($value)
 * @method static Builder|MainPageBlock whereShow($value)
 * @method static Builder|MainPageBlock whereUpdatedAt($value)
 * @method static Builder|MainPageBlock whereUrl($value)
 * @mixin Eloquent
 */
class MainPageBlock extends Model
{
    public const DESRIPTION_POSITION_VALUE = [
        'left', 'center', 'right'
    ];

    protected $table = 'main_page_blocks';
    protected $primaryKey = 'id';

    public const ID_COLUMN = 'id';
    public const NAME_COLUMN = 'name';
    public const PICTURE_COLUMN = 'picture';
    public const DESCRIPTION_COLUMN = 'description';
    public const DESCRIPTION_POSITION_COLUMN = 'description_position';
    public const DESCRIPTION_COLOR_COLUMN = 'description_color';
    public const LINK_COLUMN = 'url';
    public const ANCHOR_LINK_COLUMN = 'anchor_link';
    public const POSITION_COLUMN = 'position';
    public const SHOW_COLUMN = 'show';
    public const CREATED_AT_COLUMN = 'created_at';
    public const UPDATED_AT_COLUMN = 'updated_at';

    protected $attributes = [
        self::NAME_COLUMN => null,
        self::DESCRIPTION_COLUMN => null,
        self::DESCRIPTION_COLOR_COLUMN => null,
        self::DESCRIPTION_POSITION_COLUMN => null,
        self::ANCHOR_LINK_COLUMN => null,
        self::POSITION_COLUMN => null,
        self::SHOW_COLUMN => false,
    ];

    protected $casts = [
        self::SHOW_COLUMN => 'boolean'
    ];

    protected $fillable = [
        self::NAME_COLUMN,
        self::PICTURE_COLUMN,
        self::DESCRIPTION_COLUMN,
        self::DESCRIPTION_POSITION_COLUMN,
        self::DESCRIPTION_COLOR_COLUMN,
        self::LINK_COLUMN,
        self::ANCHOR_LINK_COLUMN,
        self::POSITION_COLUMN,
        self::SHOW_COLUMN
    ];

    protected $dates = [
        self::CREATED_AT_COLUMN,
        self::UPDATED_AT_COLUMN
    ];

    public function setDescriptionPositionAttribute($value)
    {
        if (in_array($value, self::DESRIPTION_POSITION_VALUE)) {
            $this->attributes[self::DESCRIPTION_POSITION_COLUMN] = $value;
        } else {
            throw new Exception('Выбрано значение не из списка: ' . self::DESCRIPTION_POSITION_COLUMN);
        }
    }
}
