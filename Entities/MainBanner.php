<?php

namespace Entities;

use Eloquent;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Entities\MainBanner
 *
 * @property int $id
 * @property string $url
 * @property string|null $title
 * @property string|null $text
 * @property int|null $text_size
 * @property string|null $desktop_text_color
 * @property string|null $mobile_text_color
 * @property string|null $desktop_text_position
 * @property string|null $mobile_text_position
 * @property string|null $horizontal_text_alignment
 * @property string|null $vertical_text_alignment
 * @property string|null $button_text
 * @property string $horizontal_button_alignment
 * @property string|null $vertical_button_alignment
 * @property string $name_tablet_webp_1x
 * @property string $name_tablet_webp_2x
 * @property string $name_tablet_jpg_1x
 * @property string $name_tablet_jpg_2x
 * @property string $name_mobile_webp_1x
 * @property string $name_mobile_webp_2x
 * @property string $name_mobile_webp_3x
 * @property string $name_mobile_jpg_1x
 * @property string $name_mobile_jpg_2x
 * @property string $name_mobile_jpg_3x
 * @property bool $show
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-write mixed $h_vertical_button_alignment
 * @property-write mixed $h_vertical_text_alignment
 * @method static Builder|MainBanner newModelQuery()
 * @method static Builder|MainBanner newQuery()
 * @method static Builder|MainBanner query()
 * @method static Builder|MainBanner whereButtonText($value)
 * @method static Builder|MainBanner whereCreatedAt($value)
 * @method static Builder|MainBanner whereDesktopTextColor($value)
 * @method static Builder|MainBanner whereDesktopTextPosition($value)
 * @method static Builder|MainBanner whereHorizontalButtonAlignment($value)
 * @method static Builder|MainBanner whereHorizontalTextAlignment($value)
 * @method static Builder|MainBanner whereId($value)
 * @method static Builder|MainBanner whereMobileTextColor($value)
 * @method static Builder|MainBanner whereMobileTextPosition($value)
 * @method static Builder|MainBanner whereNameMobileJpg1x($value)
 * @method static Builder|MainBanner whereNameMobileJpg2x($value)
 * @method static Builder|MainBanner whereNameMobileJpg3x($value)
 * @method static Builder|MainBanner whereNameMobileWebp1x($value)
 * @method static Builder|MainBanner whereNameMobileWebp2x($value)
 * @method static Builder|MainBanner whereNameMobileWebp3x($value)
 * @method static Builder|MainBanner whereNameTabletJpg1x($value)
 * @method static Builder|MainBanner whereNameTabletJpg2x($value)
 * @method static Builder|MainBanner whereNameTabletWebp1x($value)
 * @method static Builder|MainBanner whereNameTabletWebp2x($value)
 * @method static Builder|MainBanner whereShow($value)
 * @method static Builder|MainBanner whereText($value)
 * @method static Builder|MainBanner whereTextSize($value)
 * @method static Builder|MainBanner whereTitle($value)
 * @method static Builder|MainBanner whereUpdatedAt($value)
 * @method static Builder|MainBanner whereUrl($value)
 * @method static Builder|MainBanner whereVerticalButtonAlignment($value)
 * @method static Builder|MainBanner whereVerticalTextAlignment($value)
 * @mixin Eloquent
 */
class MainBanner extends Model
{
    protected $table = 'main_banners';
    protected $primaryKey = 'id';

    public const ID_COLUMN = 'id';
    public const URL_COLUMN = 'url';
    public const TITLE_COLUMN = 'title';
    public const TEXT_COLUMN = 'text';
    public const TEXT_SIZE_COLUMN = 'text_size';
    public const DESKTOP_TEXT_COLOR_COLUMN = 'desktop_text_color';
    public const MOBILE_TEXT_COLOR_COLUMN = 'mobile_text_color';
    public const DESKTOP_TEXT_POSITION_COLUMN = 'desktop_text_position';
    public const MOBILE_TEXT_POSITION_COLUMN = 'mobile_text_position';
    public const HORIZONTAL_TEXT_ALIGNMENT_COLUMN = 'horizontal_text_alignment';
    public const VERTICAL_TEXT_ALIGNMENT_COLUMN = 'vertical_text_alignment';
    public const BUTTON_TEXT_COLUMN = 'button_text';
    public const HORIZONTAL_BUTTON_ALIGNMENT_COLUMN = 'horizontal_button_alignment';
    public const VERTICAL_BUTTON_ALIGNMENT_COLUMN = 'vertical_button_alignment';
    public const NAME_TABLET_WEBP_1X_COLUMN = 'name_tablet_webp_1x';
    public const NAME_TABLET_WEBP_2X_COLUMN = 'name_tablet_webp_2x';
    public const NAME_TABLET_JPG_1X_COLUMN = 'name_tablet_jpg_1x';
    public const NAME_TABLET_JPG_2X_COLUMN = 'name_tablet_jpg_2x';
    public const NAME_MOBILE_WEBP_1X_COLUMN = 'name_mobile_webp_1x';
    public const NAME_MOBILE_WEBP_2X_COLUMN = 'name_mobile_webp_2x';
    public const NAME_MOBILE_WEBP_3X_COLUMN = 'name_mobile_webp_3x';
    public const NAME_MOBILE_JPG_1X_COLUMN = 'name_mobile_jpg_1x';
    public const NAME_MOBILE_JPG_2X_COLUMN = 'name_mobile_jpg_2x';
    public const NAME_MOBILE_JPG_3X_COLUMN = 'name_mobile_jpg_3x';
    public const SHOW_COLUMN = 'show';
    public const CREATED_AT_COLUMN = 'created_at';
    public const UPDATED_AT_COLUMN = 'updated_at';

    public const TEXT_POSITION_VALUE = [
        'left', 'center', 'right'
    ];
    public const VERTICAL_TEXT_ALIGNMENT_VALUE = [
        'top', 'top-mid', 'center', 'bottom-mid', 'bottom'
    ];
    public const VERTICAL_BUTTON_ALIGNMENT_VALUE = [
        'center', 'bottom'
    ];
    public const HORIZONTAL_ALIGNMENT_VALUE = [
        'left', 'left-mid', 'center', 'right-mid', 'right'
    ];

    protected $attributes = [
        self::TITLE_COLUMN => null,
        self::TEXT_COLUMN => null,
        self::TEXT_SIZE_COLUMN => null,
        self::DESKTOP_TEXT_COLOR_COLUMN => null,
        self::MOBILE_TEXT_COLOR_COLUMN => null,
        self::DESKTOP_TEXT_POSITION_COLUMN => null,
        self::MOBILE_TEXT_POSITION_COLUMN => null,
        self::HORIZONTAL_TEXT_ALIGNMENT_COLUMN => null,
        self::VERTICAL_TEXT_ALIGNMENT_COLUMN => null,
        self::BUTTON_TEXT_COLUMN => null,
        self::HORIZONTAL_BUTTON_ALIGNMENT_COLUMN => null,
        self::VERTICAL_BUTTON_ALIGNMENT_COLUMN => null,
        self::SHOW_COLUMN => false
    ];

    protected $casts = [
        self::SHOW_COLUMN => 'boolean'
    ];

    protected $fillable = [
        self::URL_COLUMN,
        self::TITLE_COLUMN,
        self::TEXT_COLUMN,
        self::TEXT_SIZE_COLUMN,
        self::DESKTOP_TEXT_COLOR_COLUMN,
        self::MOBILE_TEXT_COLOR_COLUMN,
        self::DESKTOP_TEXT_POSITION_COLUMN,
        self::MOBILE_TEXT_POSITION_COLUMN,
        self::HORIZONTAL_TEXT_ALIGNMENT_COLUMN,
        self::VERTICAL_TEXT_ALIGNMENT_COLUMN,
        self::BUTTON_TEXT_COLUMN,
        self::HORIZONTAL_BUTTON_ALIGNMENT_COLUMN,
        self::VERTICAL_BUTTON_ALIGNMENT_COLUMN,
        self::NAME_TABLET_WEBP_1X_COLUMN,
        self::NAME_TABLET_WEBP_2X_COLUMN,
        self::NAME_TABLET_JPG_1X_COLUMN,
        self::NAME_TABLET_JPG_2X_COLUMN,
        self::NAME_MOBILE_WEBP_1X_COLUMN,
        self::NAME_MOBILE_WEBP_2X_COLUMN,
        self::NAME_MOBILE_WEBP_3X_COLUMN,
        self::NAME_MOBILE_JPG_1X_COLUMN,
        self::NAME_MOBILE_JPG_2X_COLUMN,
        self::NAME_MOBILE_JPG_3X_COLUMN,
        self::SHOW_COLUMN,
    ];

    protected $dates = [
        self::CREATED_AT_COLUMN,
        self::UPDATED_AT_COLUMN
    ];

    /** Проверяет соответствует ли заданное значение
     * списку возможных
     * @param string|null $value
     * @return void|Exception
     */
    public function setDesktopTextPositionAttribute(?string $value)
    {
        if (in_array($value, self::TEXT_POSITION_VALUE)) {
            $this->attributes[self::DESKTOP_TEXT_POSITION_COLUMN] = $value;
        } else {
            throw new Exception('Выбрано значение не из списка');
        }
    }

    /** Проверяет соответствует ли заданное значение
     * списку возможных
     * @param string|null $value
     * @return void|Exception
     * @throws Exception
     */
    public function setMobileTextPositionAttribute(?string $value)
    {
        if (in_array($value, self::TEXT_POSITION_VALUE)) {
            $this->attributes[self::MOBILE_TEXT_POSITION_COLUMN] = $value;
        } else {
            throw new Exception('Выбрано значение не из списка');
        }
    }

    /** Проверяет соответствует ли заданное значение
     * списку возможных
     * @param string|null $value
     * @return void|Exception
     */

    public function setHorizontalTextAlignmentAttribute(?string $value)
    {
        if (in_array($value, self::HORIZONTAL_ALIGNMENT_VALUE)) {
            $this->attributes[self::HORIZONTAL_TEXT_ALIGNMENT_COLUMN] = $value;
        } else {
            throw new Exception('Выбрано значение не из списка');
        }
    }

    /** Проверяет соответствует ли заданное значение
     * списку возможных
     * @param string|null $value
     * @return void|Exception
     */
    public function setHVerticalTextAlignmentAttribute(?string $value)
    {
        if (in_array($value, self::VERTICAL_TEXT_ALIGNMENT_VALUE)) {
            $this->attributes[self::VERTICAL_TEXT_ALIGNMENT_COLUMN] = $value;
        } else {
            throw new Exception('Выбрано значение не из списка');
        }
    }

    /** Проверяет соответствует ли заданное значение
     * списку возможных
     * @param string|null $value
     * @return void|Exception
     */
    public function setHorizontalButtonAlignmentAttribute(?string $value)
    {
        if (in_array($value, self::HORIZONTAL_ALIGNMENT_VALUE)) {
            $this->attributes[self::HORIZONTAL_BUTTON_ALIGNMENT_COLUMN] = $value;
        } else {
            throw new Exception('Выбрано значение не из списка');
        }
    }

    /** Проверяет соответствует ли заданное значение
     * списку возможных
     * @param string|null $value
     * @return void|Exception
     */
    public function setHVerticalButtonAlignmentAttribute(?string $value)
    {
        if (in_array($value, self::VERTICAL_BUTTON_ALIGNMENT_VALUE)) {
            $this->attributes[self::VERTICAL_BUTTON_ALIGNMENT_COLUMN] = $value;
        } else {
            throw new Exception('Выбрано значение не из списка');
        }
    }
}
